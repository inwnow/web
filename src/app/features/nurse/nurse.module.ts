import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NurseRoutingModule } from './nurse-routing.module';
import { NurseComponent } from './nurse.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    NurseComponent
  ],
  imports: [
    CommonModule,
    NurseRoutingModule,
    NgZorroModule,
    SharedModule,

  ]
})
export class NurseModule { }
